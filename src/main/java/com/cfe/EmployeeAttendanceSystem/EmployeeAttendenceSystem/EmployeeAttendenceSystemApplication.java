package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeAttendenceSystemApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(EmployeeAttendenceSystemApplication.class, args);
	}
}
