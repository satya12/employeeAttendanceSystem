package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.service;

import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.AttendanceTimeRecord;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Dto.EmployeeDto;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Employee;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.repositories.EmployeeRepository;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.repositories.TimeRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TimeRecordRepository timeRecordRepository;


    @Override
    @Transactional
    public void saveEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public AttendanceTimeRecord getEmployeeDetailsByEmpCode(String empCode, Timestamp fromTime, Timestamp endTime)
    {
        Query query =em.createQuery("from AttendanceTimeRecord where empCode=:empCode and inTime between :fromTime and :endTime");
        query.setParameter("empCode", empCode);
        query.setParameter("fromTime", fromTime);
        query.setParameter("endTime", endTime);
        List<AttendanceTimeRecord> employeeList = query.getResultList();
        if (CollectionUtils.isEmpty(employeeList)) {
            return null;
        }

        return employeeList.get(0);

    }

    @Override
    public List<EmployeeDto> getEmployeeDetailsByTime(Timestamp fromTime, Timestamp endTime)
    {

        StringBuilder fetchEmpQquery = new StringBuilder();
        fetchEmpQquery.append("select emp.firstName , emp.lastName, emp.empCode, atr.inTime, atr.outTime ")
                .append("from Employee emp , AttendanceTimeRecord atr ")
                .append("where emp.empCode = atr.empCode and atr.inTime between :fromTime and :endTime ");

        Query query = em.createQuery(fetchEmpQquery.toString());
        query.setParameter("fromTime", fromTime);
        query.setParameter("endTime", endTime);
        List<Object[]> resultList =  query.getResultList();
        if (CollectionUtils.isEmpty(resultList)) {
            return null;
        }
        List<EmployeeDto> employees = EmployeeDto.getEmployeesFromResultSet(resultList);


        return employees;

    }



    @Override
    public Employee getEmployeeByEmployeeCode(String empCode)
    {
        return employeeRepository.findEmployeeByEmpCode(empCode);
    }

    @Override
    @Transactional
    public void saveAttandanceTime(AttendanceTimeRecord attendanceTimeRecord)
    {

        timeRecordRepository.save(attendanceTimeRecord);

    }

}






























//
//    @Override
//    public List<Employee> getAllEmployees()
//    {
//        return employeeRepository.findAll();
//    }


