package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "employee")
public class Employee implements Serializable
{
    private static final long serialVersionUID = 7526471155622776147L;

//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "email")
    private String email;

    @Id
    @Column(name = "emp_code",unique = true)
    private String empCode;

    @OneToMany(mappedBy = "employee" ,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<AttendanceTimeRecord> attendanceTimeRecord;

   public Employee()
    {

    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public List<AttendanceTimeRecord> getAttendanceTimeRecord() {
        return attendanceTimeRecord;
    }

    public void setAttendanceTimeRecord(List<AttendanceTimeRecord> attendanceTimeRecord) {
        this.attendanceTimeRecord = attendanceTimeRecord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Employee{");
        sb.append("emp_code=").append(empCode);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", MobileNumber='").append(mobileNumber).append('\'');
        sb.append(", email='").append(email).append('\'');
        /*sb.append(", empCode='").append(empCode).append('\'');*/
        sb.append('}');
        return sb.toString();
    }
}
