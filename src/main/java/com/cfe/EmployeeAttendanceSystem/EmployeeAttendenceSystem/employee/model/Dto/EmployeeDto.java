package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDto implements Serializable {

    private String firstName;
    private String lastName;
    private String empCode;
    private Timestamp inTime;
    private Timestamp outTime;

    public EmployeeDto() {

    }

    public EmployeeDto(String firstName, String lastName, String empCode, Timestamp inTime, Timestamp outTime) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.empCode = empCode;
        this.inTime = inTime;
        this.outTime = outTime;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public Timestamp getInTime() {
        return inTime;
    }

    public void setInTime(Timestamp inTime) {
        this.inTime = inTime;
    }

    public Timestamp getOutTime() {
        return outTime;
    }

    public void setOutTime(Timestamp outTime) {
        this.outTime = outTime;
    }

    public static List<EmployeeDto>getEmployeesFromResultSet(List<Object[]> resultSet) {
        List<EmployeeDto> employees = new ArrayList<>();
        for (Object[] object : resultSet) {
            EmployeeDto employee = new EmployeeDto((String) object[0], (String) object[1], (String) object[2], (Timestamp) object[3], (Timestamp) object[4]);
            employees.add(employee);
        }

        return employees;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EmployeeDto{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", empCode='").append(empCode).append('\'');
        sb.append(", inTime=").append(inTime);
        sb.append(", outTime=").append(outTime);
        sb.append('}');
        return sb.toString();
    }
}
