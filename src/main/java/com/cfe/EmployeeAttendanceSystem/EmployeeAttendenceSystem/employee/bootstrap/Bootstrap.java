//package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.bootstrap;
//
//import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Employee;
//import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.repositories.EmployeeRepository;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {
//    private EmployeeRepository employeeRepository;
//
//    public Bootstrap(EmployeeRepository employeeRepository) {
//        this.employeeRepository = employeeRepository;
//    }
//
//    @Override
//    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
//    {
//        employeeRepository.save(getEmployee());
//
//    }
//
//    public List<Employee> getEmployee()
//    {
//        List<Employee>employees=new ArrayList<>();
//
//        Employee employee1 = new Employee();
//        employee1.setFirstName("Rohan");
//        employee1.setLastName("Raman");
//        employee1.setEmpCode("CFE_10");
//        employee1.setEmail("rohan124@gmail.com");
//        employee1.setMobileNumber("8285447857");
//        employee1.setPassword("Rohan");
//        employee1.setConfirmPassword("Rohan");
//        employees.add(employee1);
//
//        Employee employee2=new Employee();
//        employee2.setFirstName("Sanjeet");
//        employee2.setLastName("Kumar");
//        employee2.setEmpCode("CFE_11");
//        employee2.setEmail("sanjeet12@gmail.com");
//        employee2.setMobileNumber("9285447875");
//        employees.add(employee2);
//
//       return employees;
//
//
//
//    }
//}
