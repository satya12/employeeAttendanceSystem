package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name ="attendance_time_record")
public class AttendanceTimeRecord implements Serializable
{
    private static final long serialVersionUID = 7526471155622776147L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
//    @Column(name = "attendance_record_id")
//    private int attendanceRecordId;

    //   /* @Column(name = "emp_code")
//    private String empCode;
//*/
    @Column(name = "emp_code")
    private String empCode;

    @Column(name = "in_time")
    private Timestamp inTime;

    @Column(name = "out_time")
    private Timestamp outTime;

    @ManyToOne(optional=false, fetch = FetchType.LAZY)
    @JoinColumn(name="emp_code", insertable=false, updatable=false)
    private Employee employee;

    public AttendanceTimeRecord()
    {
    }

    public AttendanceTimeRecord(String empCode) {
        this.empCode = empCode;
    }

    public AttendanceTimeRecord(Long id, String empCode, Timestamp inTime, Timestamp outTime, Employee employee) {
        this.id = id;
        this.empCode = empCode;
        this.inTime = inTime;
        this.outTime = outTime;
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public Timestamp getInTime() {
        return inTime;
    }

    public void setInTime(Timestamp inTime) {
        this.inTime = inTime;
    }

    public Timestamp getOutTime() {
        return outTime;
    }

    public void setOutTime(Timestamp outTime) {
        this.outTime = outTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AttendanceTimeRecord{");
        sb.append(", empCode='").append(empCode).append('\'');
        sb.append(", inTime=").append(inTime);
        sb.append(", outTime=").append(outTime);
        sb.append('}');
        return sb.toString();
    }
}
