package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.repositories;

import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.AttendanceTimeRecord;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface EmployeeRepository extends JpaRepository<Employee,Long>
{

    Employee findEmployeeByEmpCode(String empCode);

}
