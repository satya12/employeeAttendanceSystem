package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.repositories;

import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.AttendanceTimeRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeRecordRepository extends JpaRepository<AttendanceTimeRecord,Integer>
{

}
