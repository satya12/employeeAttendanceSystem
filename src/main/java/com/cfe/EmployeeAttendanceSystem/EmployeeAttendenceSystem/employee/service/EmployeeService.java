package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.service;

import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.AttendanceTimeRecord;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Dto.EmployeeDto;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Employee;

import java.sql.Timestamp;
import java.util.List;

public interface EmployeeService
{
    //List<Employee> getAllEmployees();

    void saveEmployee(Employee employee);

    Employee getEmployeeByEmployeeCode(String empCode);

    void saveAttandanceTime(AttendanceTimeRecord attendanceTimeRecord);

    AttendanceTimeRecord getEmployeeDetailsByEmpCode(String empCode, Timestamp fromTime, Timestamp endTime);

    List<EmployeeDto> getEmployeeDetailsByTime(Timestamp fromTime, Timestamp endTime);



}
