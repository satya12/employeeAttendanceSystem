package com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.controller;

import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.common.utils.DateUtilClass;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.AttendanceTimeRecord;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Dto.EmployeeDto;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.model.Employee;
import com.cfe.EmployeeAttendanceSystem.EmployeeAttendenceSystem.employee.service.EmployeeServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/employee/")
public class EmployeeController {

    private static final Date START_OF_DAY = DateUtilClass.getStartOfDay(new Date(System.currentTimeMillis()));
    private static final Date END_OF_DAY = DateUtilClass.getEndOfDay(new Date(System.currentTimeMillis()));


    @Autowired
    private EmployeeServiceImpl employeeService;

    private Logger log = LoggerFactory.getLogger(this.getClass());



    @RequestMapping(value = "saveEmpDetails", method = RequestMethod.GET)
    public String showEmployeeForm(Model model)
    {
        model.addAttribute("employee", new Employee());
        return "EmpDetails";
    }
    @RequestMapping(value = "saveEmpDetails", method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute Employee employee, Model model) {
        model.addAttribute("employee" , employee);
        log.debug(employee.toString());
        employeeService.saveEmployee(employee);
        return "EmployeeSavedSuccessfully";
    }


    @RequestMapping(value = "saveTimeDetails", method=RequestMethod.GET)
    public String inTime(Model model)
    {
        List<EmployeeDto> employees = employeeService.getEmployeeDetailsByTime(DateUtilClass.getTimestampFromDate(START_OF_DAY), DateUtilClass.getTimestampFromDate(END_OF_DAY));

//        for (EmployeeDto employees1:employees)
//        {
//            System.out.println(employees1);
//            }

        model.addAttribute("attendanceTimeRecord", new AttendanceTimeRecord());
        model.addAttribute("isOutTime","false");
        model.addAttribute("employeesList", employees);
        return "TimeDetails";
    }


    @RequestMapping(value = "saveInTime", method = RequestMethod.POST)
    public String getEmployeeInTime(@ModelAttribute AttendanceTimeRecord attendanceTimeRecord, Model model)
    {
        if(attendanceTimeRecord.getEmpCode()!=null && !attendanceTimeRecord.getEmpCode().isEmpty())
        {
            Employee employee = employeeService.getEmployeeByEmployeeCode(attendanceTimeRecord.getEmpCode());//valid empcode

            if (employee==null)
            {
                System.out.println("Please check your employee code. We did not find any record corresponding to this.");
                model.addAttribute("success","false");
                model.addAttribute("isOutTime","false");

            }
            else
            {
                AttendanceTimeRecord attendanceTimeRecord1 = employeeService.getEmployeeDetailsByEmpCode(attendanceTimeRecord.getEmpCode(), DateUtilClass.getTimestampFromDate(START_OF_DAY), DateUtilClass.getTimestampFromDate(END_OF_DAY));

                if(attendanceTimeRecord1!=null)
                {
                    System.out.println("in Time punched");
                    model.addAttribute("inTimePunched","true");


                }
                else
                {
                    attendanceTimeRecord.setInTime(new Timestamp(System.currentTimeMillis()));
                    employeeService.saveAttandanceTime(attendanceTimeRecord);
                    model.addAttribute("success","true");
                    model.addAttribute("isOutTime", "false");

                    }

            }

            List<EmployeeDto> employeeDtoList=employeeService.getEmployeeDetailsByTime(DateUtilClass.getTimestampFromDate(START_OF_DAY), DateUtilClass.getTimestampFromDate(END_OF_DAY));
model.addAttribute("employeesList",employeeDtoList);
            return "TimeDetails";
        }
//        else
//        {
//            System.out.println("Please fill employee code before.");
//
//        }
        return null;
    }


    @RequestMapping(value = "saveOutTime",method = RequestMethod.POST)
    public String getEmployeeOutTime(@ModelAttribute AttendanceTimeRecord attendanceTimeRecord, Model model) {
        if (attendanceTimeRecord.getEmpCode()!=null && !attendanceTimeRecord.getEmpCode().isEmpty()) {
            Employee employee = employeeService.getEmployeeByEmployeeCode(attendanceTimeRecord.getEmpCode());
            if (employee == null)
            {
                System.out.println("Please check your employee code. We did not find any record corresponding to this.");
                model.addAttribute("isOutTime","true");
                model.addAttribute("success","false");
            }
            else
            {
                AttendanceTimeRecord attendanceTimeRecord1 = employeeService.getEmployeeDetailsByEmpCode(attendanceTimeRecord.getEmpCode(), DateUtilClass.getTimestampFromDate(START_OF_DAY), DateUtilClass.getTimestampFromDate(END_OF_DAY));
                if (attendanceTimeRecord1==null)
                {
                    model.addAttribute("inTimeNotAvailable","true");
                    // model.addAttribute("success","false");
                } else if (attendanceTimeRecord1.getOutTime() != null)
                {
                    System.out.println("outTime Already punched");
                    model.addAttribute("OutTimeAlreadyPunched", "true");
                    //model.addAttribute("success","false");
                } else if (attendanceTimeRecord1.getOutTime()== null)
                {
                    attendanceTimeRecord1.setOutTime(new Timestamp(System.currentTimeMillis()));
                    employeeService.saveAttandanceTime(attendanceTimeRecord1);
                    model.addAttribute("success", "true");
                    model.addAttribute("isOutTime", "true");

                }

            }
            return "TimeDetails";
        }
//        else
//        {
//            System.out.println("Please fill employee code");
//        }

        return null;
    }
}


